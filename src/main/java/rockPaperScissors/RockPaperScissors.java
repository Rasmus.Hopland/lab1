package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    public final Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while(true) { //Game loop, break out when done. Do this to avoid System.exit(0)
            System.out.println("Let's play round " + roundCounter);
            String playerMove;
            while(true) { //Player Input loop
                playerMove = readInput("Your choice (Rock/Paper/Scissors)?");
                if (playerMove.equals("rock") || playerMove.equals("scissors") || playerMove.equals("paper")) {
                    break;
                } else {
                System.out.println("I do not understand " + playerMove + " Could you try again?");
                }
            }
            String computerMove = rpsChoices.get(new Random().nextInt(rpsChoices.size()));

            //Strings to return to player
            String computerWins = "Human chose " + playerMove + ", computer chose " + computerMove + ". Computer wins!";
            String humanWins = "Human chose " + playerMove + ", computer chose " + computerMove + ". Human wins!";
            String tied = "Human chose " + playerMove + ", computer chose " + computerMove + ". It's a tie!";

            //Checking winner
            if (playerMove.equals(computerMove)) {
                roundCounter++;
                System.out.println(tied);
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            } else if (playerMove.equals("rock")) {
                if (computerMove.equals("paper")) {
                    roundCounter++;
                    computerScore++;
                    System.out.println(computerWins);
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                } else if (computerMove.equals("scissors")) {
                    humanScore++;
                    roundCounter++;
                    System.out.println(humanWins);
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
            } else if (playerMove.equals("paper")) {
                if (computerMove.equals("scissors")) {
                    roundCounter++;
                    computerScore++;
                    System.out.println(computerWins);
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                } else if (computerMove.equals("rock")) {
                    humanScore++;
                    roundCounter++;
                    System.out.println(humanWins);
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
            } else if (playerMove.equals("scissors")) {
                if (computerMove.equals("rock")) {
                    roundCounter++;
                    computerScore++;
                    System.out.println(computerWins);
                    System.out.println("Score: human " + humanScore + " computer " + computerScore);  
                } else if (computerMove.equals("paper")) {
                    humanScore++;
                    roundCounter++;
                    System.out.println(humanWins);
                    System.out.println("Score: human " + humanScore + " computer " + computerScore);
                }
            }

            //If y or yes, go again. Any other input, break game loop.
            String playAgain = readInput("Do you wish to continue playing? (y/n)?");
                if (playAgain.equals("y") || playAgain.equals("yes")) {
                    continue;
                } else {
                    System.out.println("Bye bye :)");
                    break;
                    
                }

    }
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        userInput = userInput.toLowerCase();
        return userInput;
    }

}
